#ifndef OPENXUM_AI_SPECIFIC_YAVALAX_MCTS_PLAYER_HPP
#define OPENXUM_AI_SPECIFIC_YAVALAX_MCTS_PLAYER_HPP

#include <openxum/ai/common/mcts_player.hpp>

namespace openxum {
    namespace ai {
        namespace specific {
            namespace yavalax {

                class MCTSPlayer : public openxum::ai::common::MCTSPlayer {
                public:
                    MCTSPlayer(int c, int o, openxum::core::common::Engine* e, unsigned int simulation_number, bool stoppable)
                            :openxum::ai::common::MCTSPlayer(c, o, e, simulation_number, stoppable) { }
                };

            }
        }
    }
}

#endif