#ifndef OPENXUM_AI_SPECIFIC_YAVALAX_RANDOM_PLAYER_HPP
#define OPENXUM_AI_SPECIFIC_YAVALAX_RANDOM_PLAYER_HPP

#include <openxum/ai/common/random_player.hpp>

namespace openxum {
    namespace ai {
        namespace specific {
            namespace yavalax {

                class RandomPlayer : public openxum::ai::common::RandomPlayer {
                public:
                    RandomPlayer(int c, int o, openxum::core::common::Engine* e)
                            :openxum::ai::common::RandomPlayer(c, o, e) { }
                };

            }
        }
    }
}

#endif