#include <openxum/core/games/yavalax/Move.hpp>
#include <openxum/core/games/yavalax/Coordinates.hpp>
#include <openxum/core/games/yavalax/Color.hpp>

namespace openxum {
    namespace core {
        namespace games {
            namespace yavalax {

                Move::Move(const MoveType::Values& type, const int color, const Coordinates& coord)
                        :_type(type), _color(color), _coord(coord) { }

                openxum::core::common::Move* Move::clone() const
                {
                    return new Move(_type, _color, _coord);
                }

                void Move::decode(const std::string& str)
                {
                    if(str[0] == 'P') {
                        _type = MoveType::PUT_STONE;
                    }

                    if (str[1] == 'W') {
                        _color = Color::WHITE;
                    }else if(str[1] == 'B'){
                        _color = Color::BLUE;
                    }else if(str[1] == 'N'){
                        _color = Color::NONE;
                    }else{
                        _color = Color::EQUAL;
                    }
                    _coord = Coordinates(std::stoi(str.substr(2,2)), std::stoi(str.substr(4,2)));
                }

                std::string Move::encode() const
                {
                    return "P" +
                        std::string(_color == Color::WHITE ? "W" : (_color == Color::BLUE ? "B" : (_color == Color::NONE ? "N" : "E"))) +
                        (std::string(_coord.get_line() > 9 ? "" : "0" + _coord.get_line()) +
                        std::string(_coord.get_column() > 9 ? "" : "0" + _coord.get_column())
                    );
                }

                void Move::from_object(const nlohmann::json& json)
                {
                    Coordinates coord;

                    coord.from_object(json["coord"]);
                    _type = MoveType::Values(json["type"].get<int>());
                    _color = Color(json["color"].get<int>());
                    _coord = coord;
                }

                nlohmann::json Move::to_object() const
                {
                    nlohmann::json json;

                    json["type"] = _type;
                    json["color"] = _color;
                    json["coord"] = _coord.to_object();
                    return json;
                }

                std::string Move::to_string() const
                {
                    return "Put " + std::string(_color == Color::WHITE ? "white" : (_color == Color::BLUE ? "blue" : "invalid color")) + " piece at " + _coord.to_string();                        
                }

            }
        }
    }
}