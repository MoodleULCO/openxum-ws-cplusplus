#ifndef OPENXUM_CORE_GAMES_YAVALAX_COLOR_HPP
#define OPENXUM_CORE_GAMES_YAVALAX_COLOR_HPP

namespace openxum {
    namespace core {
        namespace games {
            namespace yavalax {
                enum Color {
                    NONE = -1, WHITE = 0, BLUE = 1, EQUAL = 2
                };
            }
        }
    }
}

#endif