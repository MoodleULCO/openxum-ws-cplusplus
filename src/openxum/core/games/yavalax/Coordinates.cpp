#include <string>

#include <openxum/core/games/yavalax/Coordinates.hpp>

namespace openxum {
    namespace core {
        namespace games {
            namespace yavalax {
       
                Coordinates::Coordinates(int c, int l) 
                    : _column(c), _line(l){}         
            }
        }
    }
}
