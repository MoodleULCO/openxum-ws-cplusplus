#ifndef OPENXUM_CORE_GAMES_YAVALAX_MOVETYPE_HPP
#define OPENXUM_CORE_GAMES_YAVALAX_MOVETYPE_HPP

namespace openxum {
    namespace core {
        namespace games {
            namespace yavalax {

                class MoveType { 
                public:
                    enum Values {
                        PUT_STONE = 0
                    };
                };
            }
        }
    }
}

#endif