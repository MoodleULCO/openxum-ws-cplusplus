#ifndef OPENXUM_CORE_GAMES_YAVALAX_MOVE_HPP
#define OPENXUM_CORE_GAMES_YAVALAX_MOVE_HPP

#include <openxum/core/common/move.hpp>
#include <openxum/core/games/yavalax/Color.hpp>
#include <openxum/core/games/yavalax/Coordinates.hpp>
#include <openxum/core/games/yavalax/MoveType.hpp>

namespace openxum {
    namespace core {
        namespace games {
            namespace yavalax {

                class Move : public openxum::core::common::Move {
                public:
                    Move() = default;

                    Move(const MoveType::Values& type, const int color, const Coordinates& coord);

                    openxum::core::common::Move* clone() const override;

                    const int get_color() const { return _color; }

                    void decode(const std::string&) override;

                    std::string encode() const override;

                    void from_object(const nlohmann::json&) override;

                    const Coordinates& get_coord() const { return _coord; }

                    nlohmann::json to_object() const override;

                    std::string to_string() const override;

                    MoveType::Values get_type() const { return _type; }

                private:
                    MoveType::Values _type;
                    int _color;
                    Coordinates _coord;
                };

                class Moves : public std::vector<Move*>
                {
                public:
                    Moves() = default;

                    virtual ~Moves() {
                        for (auto move: *this) {
                            delete move;
                        }
                    }

                    Moves& operator=(const Moves& m) {
                        for (auto move: *this) {
                            delete move;
                        }
                        for (auto move: m) {
                            push_back(move->clone());
                        }
                        return *this;
                    }
                };

            }
        }
    }
}

#endif