#ifndef OPENXUM_CORE_GAMES_YAVALAX_GAME_TYPE_HPP
#define OPENXUM_CORE_GAMES_YAVALAX_GAME_TYPE_HPP

namespace openxum {
    namespace core {
        namespace games {
            namespace yavalax {

                enum GameType {
                    STANDARD
                };

            }
        }
    }
}

#endif