#ifndef OPENXUM_CORE_GAMES_YAVALAX_CORDINATES_HPP
#define OPENXUM_CORE_GAMES_YAVALAX_CORDINATES_HPP

#include <nlohmann/json.hpp>
#include <string>

namespace openxum {
    namespace core {
        namespace games {
            namespace yavalax {

                class Coordinates {
                public:
                    explicit Coordinates(int = -1, int = -1);

                    bool is_valid(){
                        if(_line >= 0 && _line < 13 && _column >= 0 && _column < 13){
                            return true;
                        }
                        return false;
                    }
                    
                    int get_column() const { return _column; };

                    int get_line() const { return _line; };

                    bool equals(Coordinates c) {
                        if(c.get_column() == _column && c.get_line() == _line) {
                            return true;
                        }
                        return false;
                    } 

                    void from_object(const nlohmann::json& json)
                    {
                        _column = json["column"].get<int>();
                        _line = json["line"].get<int>();
                    }

                    nlohmann::json to_object() const
                    {
                        nlohmann::json json;

                        json["column"] = _column;
                        json["line"] = _line;
                        return json;
                    }

                    std::string to_string() const { return std::to_string(_column) + std::to_string(_line); };
                
                private:
                    int _column;
                    int _line;
                };
            }
        }
    }
}

#endif