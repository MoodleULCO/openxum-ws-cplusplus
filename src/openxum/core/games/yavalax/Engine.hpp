#ifndef OPENXUM_CORE_GAMES_YAVALAX_ENGINE_HPP
#define OPENXUM_CORE_GAMES_YAVALAX_ENGINE_HPP

#include <openxum/core/common/engine.hpp>

#include <openxum/core/games/yavalax/Color.hpp>
#include <openxum/core/games/yavalax/MoveType.hpp>
#include <openxum/core/games/yavalax/Move.hpp>
#include <openxum/core/games/yavalax/Coordinates.hpp>

#include <string>
#include <vector>

namespace openxum {
    namespace core {
        namespace games {
            namespace yavalax {

                class Engine : public openxum::core::common::Engine {
                public:
                    Engine() = default;

                    Engine(int type, int color);

                    ~Engine() override {
                        for (int i = 0; i < 13; i++) {
                                _gameBoard.clear();
                        }
                        
                    }

                    openxum::core::common::Move* build_move() const override { return new Move(); }

                    /*Engine* clone() const override {
                        auto e = new Engine(_type, _current_color);

                        e->_type = _type;
                        e->_current_color = _current_color;
                        e->_white_stones = _white_stones;
                        e->_blue_stones = _blue_stones;
                        e->_last_move = _last_move;
                        e->_turn_player = _turn_player;
                        e->_turn = _turn;

                        for (int l = 0; l < 13; ++l) {
                            for (int c = 0; c < 13; ++c) {
                                e->_gameBoard[l][c] = _gameBoard[l][c];
                            }
                        }

                        return e;
                    }*/

                    int current_color() const override
                    { return _current_color; }

                    const std::string& get_name() const override { return _game_name; }

                    openxum::core::common::Moves get_possible_move_list() const override;

                    bool is_finished() const override;

                    void move(const openxum::core::common::Move* move);

                    void parse(const std::string&) override { }

                    std::string to_string() const override;

                    int check_direction(Coordinates cell, int horizontal_offset, int vertical_offset) const;

                    bool check_align(Coordinates cell) const;

                    bool check_fifth_align(Coordinates cell) const;

                    int count_align(Coordinates cell) const;

                    int winner_is() const override;

                    bool in_possible_move(Coordinates cell) const;

                private:
                    int _type;
                    int _current_color;
                    std::string _game_name = "Yavalax";
                    int _white_stones = 85;
                    int _blue_stones = 85;
                    Move _last_move = Move(MoveType::PUT_STONE, Color::NONE, Coordinates(-1,-1));
                    int _turn_player = 0;
                    int _turn = 0;

                    std::vector<std::vector<int>> _gameBoard/*(13, std::vector<int>(13,Color::NONE))*/;

                    void _put_stone(const Coordinates selected_cell);

                };

            }
        }
    }
}

#endif