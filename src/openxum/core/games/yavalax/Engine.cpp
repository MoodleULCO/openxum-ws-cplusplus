#include <openxum/core/games/yavalax/Engine.hpp>
#include <openxum/core/games/yavalax/Move.hpp>

#include <vector>
#include <string>

namespace openxum {
    namespace core {
        namespace games {
            namespace yavalax {
                    Engine::Engine(int type, int color) : _type(type), _current_color(color) { 
                        for(int i = 0; i < 13; i++){
                            for(int j = 0; j < 13; j++) {
                                _gameBoard.push_back(Color::NONE);
                            }
                        }
                    }

                    openxum::core::common::Moves Engine::get_possible_move_list() const {
                        openxum::core::common::Moves list;
                        Coordinates coord;
                        int color = current_color();

                        for(int i = 0; i < 13; i++){
                            for(int j = 0; j < 13; j++){
                                coord = Coordinates(i,j);

                                if(!is_finished() && _gameBoard[i][j] == Color::NONE
                                    && coord.is_valid() && !check_fifth_align(coord)
                                    && count_align(coord) != 1){

                                    list.push_back(new Move(MoveType::PUT_STONE,color,coord));
                                }
                            }
                        }
                        return list;
                    }

                    bool Engine::is_finished() const {
                        if(winner_is() != -1){
                            return true;
                        }
                        return false;
                    }

                    void Engine::move(const openxum::core::common::Move* move){
                        auto* m = dynamic_cast<const openxum::core::games::yavalax::Move*>(move);

                        if (m->get_type() == MoveType::PUT_STONE) {
                            _put_stone(m->get_coord());

                            if(!is_finished()){
                                if(_turn_player == 2 && current_color() == Color::BLUE){
                                    _current_color = Color::WHITE;
                                    _turn_player = 0;
                                }else if(_turn_player == 2 && current_color() == Color::WHITE){
                                    _current_color = Color::BLUE;
                                    _turn_player = 0;
                                }
                            }
                        }
                    }

                    std::string Engine::to_string() const {
                        std::string str = " ";

                        for (int _line = 0; _line < _gameBoard.size(); _line++) {
                            if (_line < 9) {
                                str += (_line + 1) + " ";
                            } else {
                                str += (_line + 1) + " ";
                            }
                            for (int _column = 0; _column < _gameBoard.size(); _column++) {
                                str += " " + _gameBoard[_line][_column]+ ' ';
                            }
                            str += "\n";
                        }
                        return str;
                    }

                    int Engine::check_direction(const Coordinates cell, int horizontal_offset, int vertical_offset) const {
                        int number_align = 1;
                        int column = cell.get_column();
                        int line = cell.get_line();
                        int color = current_color();

                        while( ((column + horizontal_offset) >= 0 && (column + horizontal_offset) < 13) &&
                            ((line + vertical_offset) >= 0 && (line + vertical_offset) < 13) &&
                                _gameBoard[line + vertical_offset][column + horizontal_offset] == color ) {
                            number_align++;
                            column = column + horizontal_offset;
                            line = line + vertical_offset;
                        }
                        return number_align;
                    }

                    bool Engine::check_align(const Coordinates cell) const {
                        if(count_align(cell) >= 2){
                            return true;
                        }
                        return false;
                    }

                    bool Engine::check_fifth_align(const Coordinates cell) const {
                        int align_line = check_direction(cell, 1, 0) + check_direction(cell, -1, 0) -1;
                        int align_column = check_direction(cell, 0, 1) + check_direction(cell, 0, -1) -1;
                        int align_first_diag = check_direction(cell, -1, -1) + check_direction(cell, 1, 1) -1;
                        int align_second_diag = check_direction(cell, -1, 1) + check_direction(cell, 1, -1) -1;

                        if(align_line >= 5 || align_column >= 5 || align_first_diag >= 5 || align_second_diag >= 5){
                            return true;
                        }
                        return false;
                    }

                    int Engine::count_align(const Coordinates cell) const {
                        int align_line = check_direction(cell, 1, 0) + check_direction(cell, -1, 0) -1;
                        int align_column = check_direction(cell, 0, 1) + check_direction(cell, 0, -1) -1;
                        int align_first_diag = check_direction(cell, -1, -1) + check_direction(cell, 1, 1) -1;
                        int align_second_diag = check_direction(cell, -1, 1) + check_direction(cell, 1, -1) -1;

                        return (align_line == 4) + (align_column == 4) + (align_first_diag == 4) + (align_second_diag == 4);
                    }

                    int Engine::winner_is() const {
                        if ((_blue_stones == 0 && _white_stones == 0) || _turn == 168) {
                            return Color::EQUAL;
                        } else if(_turn == 0) {
                            return Color::NONE;
                        } else if(check_align(_last_move.get_coord()) == true) {
                            return _last_move.get_color();
                        }
                        return Color::NONE;
                    }

                    bool Engine::in_possible_move(const Coordinates cell) const {
                        openxum::core::common::Moves l = get_possible_move_list();
                        auto* list = dynamic_cast<const openxum::core::games::yavalax::Moves>(l);


                        for (int index = 0; index < list->size(); index ++) {
                            if (list[index]->get_coord()->equals(cell)) {
                                return true;
                            }
                        }
                        return false;
                    }
                
                    void Engine::_put_stone(const Coordinates selected_cell) {
                        if(in_possible_move(selected_cell) && selected_cell.is_valid()
                            && _gameBoard[selected_cell.get_line()][selected_cell.get_column()] == Color::NONE) {

                            _gameBoard[selected_cell.get_line()][selected_cell.get_column()] = current_color();
                            if(current_color() == Color::BLUE){
                                _blue_stones--;
                            }else{
                                _white_stones--;
                            }
                            _last_move = new Move(MoveType::PUT_STONE, current_color(),selected_cell);
                            _turn ++;
                            _turn_player++;
                        }
                    }
            }
        }
    }            
}